<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('data_penjualan', function (Blueprint $table) {
      $table->id();
      $table->foreignId('id_produksi')
        ->references('id')
        ->on('data_produksi')
        ->cascadeOnDelete();
      $table->foreignId('id_pelanggan')
        ->references('id')
        ->on('pelanggan')
        ->cascadeOnDelete();
      $table->integer('jumlah');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('data_penjualan');
  }
};
