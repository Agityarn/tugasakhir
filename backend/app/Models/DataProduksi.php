<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataProduksi extends Model
{
  use HasFactory;

  protected $table = 'data_produksi';

  protected $fillable = [
    'id_produksi',
    'id_produk',
    'jumlah_produksi',
    'terjual',
    'stok',
  ];

  public function produk()
  {
    return $this->belongsTo(Produk::class, 'id_produk');
  }
}
