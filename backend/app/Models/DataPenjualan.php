<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPenjualan extends Model
{
  use HasFactory;
  protected $table = 'data_penjualan';
  protected $fillable = [
    'id_produksi',
    'id_pelanggan',
    'jumlah',
  ];

  public function dataProduksi() {
    return $this->belongsTo(DataProduksi::class, 'id_produksi');
  }

  public function pelanggan() {
    return $this->belongsTo(Pelanggan::class, 'id_pelanggan');
  }
}
