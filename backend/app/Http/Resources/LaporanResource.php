<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LaporanResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
        'id_produksi' => $this->id,
        'data_produksi' => new DataProduksiResource($this->dataProduksi()->first()),
        'pelanggan' => $this->pelanggan()->first(),
        'terjual' => $this->jumlah,
        'waktu_penjualan' => $this->created_at
        
        ];
    }
}
