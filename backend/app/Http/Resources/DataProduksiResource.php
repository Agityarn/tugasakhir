<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DataProduksiResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'waktu_produksi' => $this->created_at,
      'id_produksi' => $this->id_produksi,
      'produk' => $this->produk->nama,
      'jumlah_produksi' => $this->jumlah_produksi,
      'terjual' => $this->terjual,
      'stok' => $this->stok,
    ];
  }
}
