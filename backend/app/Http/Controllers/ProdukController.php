<?php

namespace App\Http\Controllers;
use App\Models\Produk;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $produk = Produk::all();

    return response($produk, 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $produk = Produk::create([
      'nama' => $request->input('nama')
    ]);

    return response($produk, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Produk  $product
   * @return \Illuminate\Http\Response
   */
  public function show(Produk $product)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Produk  $product
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Produk $produk)
  {
    $produk->nama = $request->input('nama');
    $produk->save();

    return response($produk, 200);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Produk  $product
   * @return \Illuminate\Http\Response
   */
  public function destroy(Produk $produk)
  {
    $produk->delete();

    return response('', 204);
  }
}
