import { Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Box, Button } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import logo from '../../../../assets/tes.png'


export default function SideNavComponent() {
  const userInfo = localStorage.getItem("user-info");
  const user = JSON.parse(userInfo);
  const navigate = useNavigate();

  function logout() {
    localStorage.clear();
    navigate("/login");
  }

  return (
    <>
      <div className="px-2 py-8">
        <img src={logo} />
      </div>
      <Accordion allowMultiple allowToggle>
        <AccordionItem className="py-2 pl-4 text-white cursor-pointer" onClick={() => navigate('/dashboard')}>
          <div className="flex flex-row items-center gap-4 py-3">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
              <path strokeLinecap="round" strokeLinejoin="round" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
            </svg>
            Dashboard
          </div>
        </AccordionItem>
        <AccordionItem>
          <h2>
            <AccordionButton className="text-white">
              <div className="w-full flex flex-row items-center gap-4 py-3">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                </svg>
                Produk
              </div>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel className="text-white flex flex-col items-start gap-2">
            <Link to="/dashboard/input-produk">● Input Produk</Link>
            <Link to="/dashboard/data-produk">● Data Produk</Link>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem>
          <h2>
            <AccordionButton className="text-white">
              <div className="w-full flex flex-row items-center gap-4 py-3">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M16 4v12l-4-2-4 2V4M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                </svg>
                Data Produksi
              </div>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel className="text-white flex flex-col items-start gap-2">
            <Link to="/dashboard/input-data">● Input Produksi</Link>
            <Link to="/dashboard/quality-check">● Data Produksi</Link>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem>
          <h2>
            <AccordionButton className="text-white">
              <div className="w-full flex flex-row items-center gap-4 py-3">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                </svg>
                Data Penjualan
              </div>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel className="text-white flex flex-col items-start gap-2">
            <Link to="/dashboard/input-data-penjualan">● Input Penjualan</Link>
            <Link to="/dashboard/data-penjualan">● Data Penjualan</Link>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem>
          <h2>
            <AccordionButton className="text-white">
              <div className="w-full flex flex-row items-center gap-4 py-3">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253" />
                </svg>
                Data Stok
              </div>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel className="text-white flex flex-col items-start gap-2">
            <Link to="/dashboard/cek-data-stok">● Cek Data Stok</Link>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem>
          <h2>
            <AccordionButton className="text-white">
              <div className="w-full flex flex-row items-center gap-4 py-3">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                </svg>
                Pelanggan
              </div>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel className="text-white flex flex-col items-start gap-2">
            <Link to="/dashboard/data-pelanggan">● Data Pelanggan</Link>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem>
          <h2>
            <AccordionButton className="text-white">
              <div className="w-full flex flex-row items-center gap-4 py-3">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                </svg>
                Laporan
              </div>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel className="text-white flex flex-col items-start gap-2">
            <Link to="/dashboard/laporan-penjualan">● Penjualan</Link>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem>
          <h2>
            <AccordionButton className="text-white">
              <div className="w-full flex flex-row items-center gap-4 py-3">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                  <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                </svg>
                Pengaturan
              </div>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel className="text-white flex flex-col items-start gap-2">
            <Link to="/dashboard/pengaturan-user">● Pengaturan User</Link>
            {
              user
                ?
                <Button variant="primary" size="lg" onClick={logout}>
                  Logout
                </Button>
                : null
            }
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </>
  );
}