import { useEffect, useMemo, useState } from "react";
import axios from "axios";

const dummyData = [
  {
    nama: 'Kepiting',
    id_produksi: '13',
    waktu_produksi: '11 Juli 2022',
    jumlah_produksi: 20,
    terjual: 12,
    stok: 20 - 12
  },
  {
    nama: 'Cumi-Cumi',
    id_produksi: '14',
    waktu_produksi: '11 Juli 2022',
    jumlah_produksi: 92,
    terjual: 42,
    stok: 92 - 42
  },
];

export default function CekDataStokPage() {
  const [dataStok, setDataStok] = useState([]);

  useEffect(() => {
    axios
      .get('/api/data-stok')
      .then((value) => setDataStok(value.data));
  }, []);

  const dataStokElement = useMemo(() => dataStok.map((element) => (
    <tr>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.produk}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.id_produksi}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.waktu_produksi}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.jumlah_produksi}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.terjual}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.stok}</td>
    </tr>
  )), [dataStok]);

  return (
    <div className="w-full h-full p-4 font-roboto bg-gray-100">
      <h1 className="font-roboto text-4xl text-black mb-4">Data Stock</h1>
      <div className="flex felx-row w-full m-4 bg-white p-4">
        <table className="table-auto w-full">
          <thead>
            <tr>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Nama Produk</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">ID Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Waktu Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Jumlah Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Terjual</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Stock</th>
            </tr>
          </thead>
          <tbody>
            {dataStokElement}
          </tbody>
        </table>
      </div>
    </div>
  );
}