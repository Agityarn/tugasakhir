import { AlertDialog, AlertDialogBody, AlertDialogContent, AlertDialogFooter, AlertDialogHeader, AlertDialogOverlay, Button, FormControl, FormLabel, Input, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, useDisclosure, useToast } from "@chakra-ui/react";
import { useRef } from "react";
import { useMemo } from "react";
import { useEffect } from "react";
import { useState } from "react";
import axios from 'axios';

function TableRow({ no, name, role, dibuat, buttons }) {
  return (
    <tr>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{no}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left">{name}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left">{role === 1 ? 'Admin' : 'User'}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left">{dibuat}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left">{buttons}</td>
    </tr>
  )
}

export default function PengaturanUserPage() {
  const [usersData, setUsersData] = useState([]);
  const [userData, setUserData] = useState({});
  const { isOpen: isModalOpen, onOpen: onModalOpen, onClose: onModalClose } = useDisclosure();
  const { isOpen: isDialogOpen, onOpen: onDialogOpen, onClose: onDialogClose } = useDisclosure();
  const cancelRef = useRef();
  const toast = useToast();

  const [tempId, setTempId] = useState(-1);

  const fetchUserData = () => fetch('http://localhost:8000/api/users/index')
    .then((response) => response.json())
    .then((json) => setUsersData(json));

  const deleteUser = (id) => fetch('http://localhost:8000/api/users/' + id, { method: 'DELETE' })
    .then((response) => fetchUserData());

  const onOpenDialog = (id) => {
    setTempId(id);
    onDialogOpen();
  }

  const updateUser = () => {
    const form = new FormData(document.getElementById('update-form'));
    console.log('name ', form.get('name'));

    axios({
      url: 'http://localhost:8000/api/users/' + userData.id,
      method: 'PUT',
      data: {
        name: form.get('name'),
        email: form.get('email'),
        password: form.get('password'),
      }
    }).then(() => fetchUserData());
    onModalClose();
  }

  const onOpenModal = (userObject) => {
    setUserData(userObject);
    onModalOpen();
  }

  const onDelete = () => {
    const user = JSON.parse(localStorage.getItem('user-info'));
    console.log(user[['is_admin']])
    if (user['is_admin'] === 1) {
      deleteUser(tempId);
      onDialogClose()
      toast({
        title: 'Success',
        description: 'User telah dihapus',
        status: 'success',
        duration: 3000
      });
    } else {
      toast({
        title: 'Error',
        description: 'Anda bukan admin',
        status: 'error',
        duration: 3000
      });
      onDialogClose();
    }
  }

  const usersElement = useMemo(() => {

    return usersData.map((value, index) => <TableRow
      id={value.id}
      no={index + 1}
      name={value.name}
      role={value.is_admin}
      dibuat={new Date(value.created_at).toLocaleDateString()}
      buttons={<div className="flex flex-row gap-2">
        <Button marginY="1rem" marginRight="1rem" colorScheme='red' onClick={() => onOpenDialog(value.id)}>Delete User</Button>
        <Button margin="1rem" colorScheme='green' onClick={() => onOpenModal(value)}>Edit User</Button>
      </div>}
    />)
  }, [usersData]);

  useEffect(() => {
    fetchUserData();
  }, []);

  return (
    <div className="w-full h-full p-4 font-roboto bg-gray-100 overflow-y-scroll">
      <h1 className="font-roboto text-4xl text-black m-4">Pengaturan User</h1>
      <div className="flex flex-row w-full m-4 bg-white p-4">
        <table className="table-auto w-full">
          <thead>
            <tr>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">No</th>
              <th className="border-collapse border-y border-y-gray-300 text-left">Username</th>
              <th className="border-collapse border-y border-y-gray-300 text-left">Role</th>
              <th className="border-collapse border-y border-y-gray-300 text-left">Dibuat</th>
              <th className="border-collapse border-y border-y-gray-300 text-left">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {usersElement}
          </tbody>
        </table>
      </div>
      <AlertDialog
        isOpen={isDialogOpen}
        leastDestructiveRef={cancelRef}
        onClose={onDialogClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize='lg' fontWeight='bold'>
              Delete User
            </AlertDialogHeader>

            <AlertDialogBody>
              Are you sure? You can't undo this action afterwards.
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onDialogClose}>
                Cancel
              </Button>
              <Button colorScheme='red' onClick={onDelete} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <Modal
        isOpen={isModalOpen}
        onClose={onModalClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit User</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <form id="update-form">
              <FormControl>
                <FormLabel>User Name</FormLabel>
                <Input name="name" defaultValue={userData.name} />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Email Password</FormLabel>
                <Input name="email" type="email" defaultValue={userData.email} />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Password</FormLabel>
                <Input name="password" type="password" placeholder="********" />
              </FormControl>
            </form>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme='blue' mr={3} onClick={updateUser}>
              Save
            </Button>
            <Button onClick={onModalClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
}