import { FormControl, FormLabel, Input, Button, Select, useEditable, useToast } from '@chakra-ui/react';
import { useEffect } from 'react';
import { useState } from 'react';
import axios from 'axios';
import { useMemo } from 'react';

export default function InputDataPenjualanPage() {
  const toast = useToast();

  const clearForm = () => {
    document.getElementById('id_produksi').value = '';
    document.getElementById('id_pelanggan').value = '';
    document.getElementById('jumlah').value = '';
  };

  const fetchProduk = () => {
    axios
      .get('/api/produksi')
      .then((value) => {
        console.log(value);
        const filtered = value.data.filter((value) => value.stok != 0);
        setDataProduk(filtered);
      });
  }

  const [dataProduk, setDataProduk] = useState([]);

  const dataProdukElement = useMemo(() => dataProduk.map((element) => (
    <option value={element.id}>{`${element.produk} - ID Produksi: ${element.id_produksi} - Stok: ${element.stok}`}</option>
  )), [dataProduk]);

  useEffect(() => { fetchProduk() }, [])

  const [dataPelanggan, setDataPelanggan] = useState([]);

  const pelangganElement = useMemo(() => dataPelanggan.map((element) => (
    <option value={element.id}>{`${element.nama}`}</option>
  )), [dataPelanggan]);

  useEffect(() => {
    axios
      .get('/api/pelanggan')
      .then((value) => setDataPelanggan(value.data));
  }, [])

  const onSubmit = () => {
    axios
      .post('/api/penjualan', {
        id_produksi: document.getElementById('id_produksi').value,
        id_pelanggan: document.getElementById('id_pelanggan').value,
        jumlah: document.getElementById('jumlah').value,
      })
      .then((value) => {
        if (value.status === 201) {
          toast({
            title: 'Produk Berhasil Ditambahkan',
            status: 'success',
            duration: 2000,
            position: 'top'
          });
          clearForm();
          fetchProduk();
        }
      })
  }


  return (
    <div className="h-full w-full p-4">
      <h1 className="font-roboto text-4xl text-black">Input Data Penjualan</h1>
      <div className="bg-white w-full mt-12">
        <form>
          <FormControl marginY='1rem'>
            <FormLabel>Produk</FormLabel>
            <Select id="id_produksi" placeholder='Pilih Produk' >
              {dataProdukElement}
            </Select>
          </FormControl>
          <FormControl marginY='1rem'>
            <FormLabel>Pelanggan</FormLabel>
            <Select id="id_pelanggan" placeholder="Pilih Pelanggan" >
              {pelangganElement}
            </Select>
          </FormControl>
          <FormControl marginY='1rem'>
            <FormLabel>Jumlah</FormLabel>
            <Input id="jumlah" type="text" />
          </FormControl>
          <Button
            marginY="1rem"
            variant="solid"
            onClick={onSubmit}
          >
            Input Penjualan
          </Button>
        </form>
      </div>
    </div>
  );
}