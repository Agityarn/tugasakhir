import axios from "axios";
import { useMemo } from "react";
import { useState } from "react";
import { useEffect } from "react";
import { Button, useToast } from '@chakra-ui/react';

export default function DataPenjualanPage() {
  const [dataPenjualan, setDataPenjualan] = useState([]);
  const toast = useToast();

  const fetchData = () => axios
    .get('/api/penjualan')
    .then((value) => setDataPenjualan(value.data));

  useEffect(() => { fetchData() }, []);

  const dataPenjualanElemen = useMemo(() => dataPenjualan.map((element) => {
    const onDelete = () => {
      axios
        .delete('/api/penjualan/' + element.id)
        .then((value) => {
          fetchData();
          toast({
            title: 'Berhasil menghapus data penjualan',
            position: 'top',
            status: 'success',
            duration: 2000
          });
        });
    }

    return (
      <tr>
         <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.data_produksi.id_produksi}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.data_produksi.produk}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.data_produksi.waktu_produksi}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.waktu_penjualan}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.pelanggan.nama}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.jumlah}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">
          <Button colorScheme="red" onClick={onDelete}>Delete</Button>
        </td>
      </tr>
    )
  }), [dataPenjualan]);

  return (
    <div className="w-full h-full p-4 font-roboto bg-gray-100">
      <h1 className="font-roboto text-4xl text-black mb-4">Data Penjualan</h1>
      <div className="flex flex-row w-full m-4 bg-white p-4">
        <table className="table-auto w-full">
          <thead>
            <tr>
            <th className="border-collapse border-y border-y-gray-300 text-left p-2">ID Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Nama Produk</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Waktu Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Waktu Penjualan</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Pelanggan</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Jumlah</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {dataPenjualanElemen}
          </tbody>
        </table>
      </div>
    </div>
  );
}