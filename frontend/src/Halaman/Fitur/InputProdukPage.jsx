import { Button, FormControl, FormLabel, Input, useToast } from "@chakra-ui/react";
import axios from 'axios';


export default function InputProdukPage() {
  const toast = useToast();

  const submitProduk = () => axios({
    url: 'http://localhost:8000/api/products',
    method: 'POST',
    data: {
      nama: document.getElementById('nama').value
    }
  }).then((response) => {
    if (response.status === 201) {
      toast({
        title: 'Produk Berhasil Ditambahkan',
        status: 'success',
        duration: 2000,
        position: 'top'
      });
      document.getElementById('nama').value = '';
    }
  });

  return (
    <div className="h-full w-full p-4">
      <h1 className="font-roboto text-4xl text-black">Input Produk</h1>
      <div className="bg-white w-full mt-12">
        <form>
          <FormControl marginY="1rem" >
            <FormLabel htmlFor="nama">Nama Produk</FormLabel>
            <Input id="nama" type="text" />
          </FormControl>
          <Button
            marginY="1rem"
            variant="solid"
            onClick={submitProduk}
          >
            Input Produk
          </Button>
        </form>
      </div>
    </div>
  )
}