import {
  Button, useDisclosure, useToast,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
} from '@chakra-ui/react'
import axios from 'axios';
import { useState } from 'react';
import { useEffect } from 'react';

export default function DataProdukPage() {
  const [data, setData] = useState([]);
  const [tempId, setTempId] = useState(-1);

  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const fetchData = () => axios({
    url: 'http://localhost:8000/api/products',
    method: 'GET'
  }).then((response) => setData(response.data));

  useEffect(() => { fetchData() }, [])

  const onSave = () => {
    axios({
      url: 'http://localhost:8000/api/products/' + tempId,
      method: 'PUT',
      data: {
        'nama': document.getElementById('nama').value
      }
    }).then((response) => {
      if (response.status === 200) {
        toast({
          title: 'Berhasil Mengganti Nama Produk',
          duration: 2000,
          status: 'success',
          position: 'top'
        });
        fetchData();
        onClose();
      }
    });
  }

  const onEditClick = (id) => () => {
    setTempId(id);
    onOpen();
  }

  const dataProdukElement = data.map((element) => {

    const onDelete = () => axios({
      url: 'http://localhost:8000/api/products/' + element.id,
      method: 'DELETE'
    }).then(() => {
      toast({
        title: 'Berhasil Menghapus Produk',
        duration: 2000,
        status: 'success',
        position: 'top'
      });
      fetchData()
    });

    const elementD = (
      <tr>
        <td className="border-collapse border-y border-y-gray-300 text-left">{element.nama}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left flex flex-row items-center">
          <Button margin="0.5rem" colorScheme="blue" onClick={onEditClick(element.id)}>Edit</Button>
          <Button margin="0.5rem" colorScheme="red" onClick={onDelete}>Hapus</Button>
        </td>
      </tr>
    );

    return elementD;
  });

  return (
    <div className="w-full h-full p-4 font-roboto bg-gray-100">
      <h1 className="font-roboto text-4xl text-black mb-4">Data Produk</h1>
      <div className="flex flex-row w-full m-4 bg-white p-4">
        <table className="table-auto w-full">
          <thead>
            <tr>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Nama Produk</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {dataProdukElement}
          </tbody>
        </table>
      </div>
      <Modal isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Produk</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <form>
              <FormControl marginY="1rem" >
                <FormLabel htmlFor="nama">Nama Produk</FormLabel>
                <Input id="nama" type="text" />
              </FormControl>
            </form>
          </ModalBody>
          <ModalFooter>
            <Button variant='ghost' onClick={onClose}>Cancel</Button>
            <Button colorScheme='blue' ml={3} onClick={onSave}>
              Simpan
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
}