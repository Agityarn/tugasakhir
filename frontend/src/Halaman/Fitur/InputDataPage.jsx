import { Button, FormControl, FormLabel, Input, Select, useToast } from "@chakra-ui/react";
import { useState, useEffect } from "react";
import axios from 'axios';
import { useMemo } from "react";

export default function InputDataPage() {

  const [idProduksi, setIdProduksi] = useState(0);
  const [products, setProducts] = useState([]);
  const toast = useToast();

  const optionsElement = useMemo(() => {
    return products.map((element) => {
      return <option value={element.id}>{element.nama}</option>
    });
  }, [products])

  const fetchData = () => axios({
    url: 'http://localhost:8000/api/product-id',
    method: 'GET'
  }).then((value) => setIdProduksi(value.data));

  useEffect(() => { fetchData() }, []);

  const onButtonClick = () => {
    axios.post('/api/produksi', {
      id_produksi: idProduksi,
      id_produk: document.getElementById('produk').value,
      jumlah: document.getElementById('jumlah').value,
    }).then((response) => {
      fetchData();
      document.getElementById('produk').value = '';
      document.getElementById('jumlah').value = '';
      toast({
        title: 'Berhasil menambahkan data produksi',
        position: 'top',
        status: 'success',
        duration: 2000
      });
    });

  }

  useEffect(() => {
    axios({
      url: 'http://localhost:8000/api/products',
      method: 'GET'
    }).then((value) => setProducts(value.data));
  }, []);

  return (
    <div className="h-full w-full p-4">
      <h1 className="font-roboto text-4xl text-black">Input Data Produksi</h1>
      <div className="bg-white w-full mt-12">
        <form>
          <FormControl marginY="1rem">
            <FormLabel htmlFor="id-produksi">ID Produksi</FormLabel>
            <Input id="id-produksi" value={idProduksi} disabled />
          </FormControl>
          <FormControl marginY="1rem" >
            <FormLabel htmlFor="produk">Produk</FormLabel>
            <Select id="produk" placeholder="Pilih Produk">
              {optionsElement}
            </Select>
          </FormControl>
          <FormControl marginY="1rem">
            <FormLabel htmlFor="jumlah">Jumlah</FormLabel>
            <Input id="jumlah" type="number" />
          </FormControl>
          <Button
            marginY="1rem"
            variant="solid"
            onClick={onButtonClick}
          >
            Input Produksi
          </Button>
        </form>
      </div>
    </div>
  );
}
