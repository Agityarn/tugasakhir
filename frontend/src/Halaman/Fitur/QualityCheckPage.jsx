import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button, useDisclosure, useTab, useToast, FormControl, FormLabel, Input
} from '@chakra-ui/react'
import axios from "axios";
import { useMemo } from "react";
import { useState } from "react";
import { useEffect } from "react";

export default function QualityCheckPage() {

  const [dataProduksi, setDataProduksi] = useState([]);
  const [tempId, setTempId] = useState(-1);

  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const fetchData = () => axios
    .get('/api/produksi')
    .then((value) => setDataProduksi(value.data));

  useEffect(() => { fetchData() }, []);

  const onEditCallback = () => axios
    .put('/api/produksi/' + tempId, {
      jumlah_produksi: document.getElementById('jumlah_produksi'),
      terjual: document.getElementById('terjual'),
    })
    .then((value) => {
      fetchData();
      onClose();
      toast({
        title: 'Berhasil mengedit data produksi',
        position: 'top',
        status: 'success',
        duration: 2000
      });
    });

  const dataProduksiElement = useMemo(() => {
    return dataProduksi.map((element) => {
      const onDelete = () => axios
        .delete('/api/produksi/' + element.id)
        .then((value) => {
          fetchData();
          onClose();
          toast({
            title: 'Berhasil menghapus data produksi',
            position: 'top',
            status: 'success',
            duration: 2000
          });
        });

      const onEdit = () => {
        onOpen();
        setTempId(element.id)
        axios
          .get('/api/produksi/' + element.id)
          .then((value) => {
            document.getElementById('jumlah_produksi').value = value.data.jumlah_produksi;
            document.getElementById('terjual').value = value.data.terjual;
          });
      }

      return (<tr>
        <td className="p-2">{element.id_produksi}</td>
        <td className="p-2">{element.produk}</td>
        <td className="p-2">{(new Date(element.waktu_produksi)).toLocaleString()}</td>
        <td className="p-2">{element.jumlah_produksi}</td>
        <td className="p-2">{element.terjual}</td>
        <td className="p-2">{element.stok}</td>
        <td className="flex flex-row gap-2">
          <Button m="0.5rem" colorScheme="red" onClick={onDelete}>Delete</Button>
          <Button m="0.5rem" colorScheme="blue" onClick={onEdit}>Edit</Button>
        </td>
      </tr>
      )
    });
  }, [dataProduksi]);

  return (
    <div className="w-full h-full p-4 font-roboto bg-gray-100">
      <h1 className="font-roboto text-4xl text-black mb-4">Data Produksi</h1>
      <div className="flex flex-row w-full m-4 bg-white p-4">
        <table className="table-auto w-full">
          <thead>
            <tr>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">ID Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Nama Produk</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Waktu Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Jumlah Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Terjual</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Stok</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {dataProduksiElement}
          </tbody>
        </table>
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Edit Data Produksi</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <form>
                <FormControl mb="1rem">
                  <FormLabel htmlFor='jumlah_produksi'>Jumlah Produksi</FormLabel>
                  <Input id="jumlah_produksi" type="number" />
                </FormControl>
                <FormControl>
                  <FormLabel htmlFor='terjual'>Terjual</FormLabel>
                  <Input id="terjual" type="number" />
                </FormControl>
              </form>
            </ModalBody>
            <ModalFooter>
              <Button variant='ghost'>Cancel</Button>
              <Button colorScheme='blue' ml={3} onClick={onEditCallback}>
                Edit
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </div>
    </div>
  );
}