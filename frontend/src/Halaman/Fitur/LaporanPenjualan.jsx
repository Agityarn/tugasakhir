import { Button } from "@chakra-ui/react";
import { useMemo, useState, useEffect } from "react";
import axios from "axios";

export default function LaporanPenjualan() {
  const [laporan, setLaporan] = useState([]);
  useEffect(() => {
    axios
      .get('/api/penjualan')
      .then((value) => setLaporan(value.data));
  }, []);

  const laporanElement = useMemo(() => laporan.map((element) => {
    return (
      <tr>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.data_produksi.id_produksi}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.data_produksi.produk}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{(new Date(element.data_produksi.waktu_produksi)).toLocaleString()}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.pelanggan.nama}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.jumlah}</td>
      </tr>
    )
  }), [laporan]);

  return (
    <div className="w-full h-full p-4 font-roboto bg-gray-100">
      <h1 className="font-roboto text-4xl text-black mb-2"> Laporan Penjualan </h1>
      <div className="flex flex-row w-full m-4">
        <form >
          <Button colorScheme='blue' onClick={() => window.location.assign('http://localhost:8000/penjualan')}>Print</Button>
        </form>
      </div>
      <div>
        <table className="table-auto w-full">
          <thead>
            <tr>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">ID Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left">Nama Produk</th>
              <th className="border-collapse border-y border-y-gray-300 text-left">Waktu Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left">Pelanggan</th>
              <th className="border-collapse border-y border-y-gray-300 text-left">Terjual</th>
            </tr>
          </thead>
          <tbody>
            {laporanElement}
          </tbody>
        </table>
      </div>
    </div>
  )
}