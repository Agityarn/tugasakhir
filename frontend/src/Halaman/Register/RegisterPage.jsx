import { Button, Input } from "@chakra-ui/react";
import { FormControl, FormLabel } from "@chakra-ui/react";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import SideNavComponent from "../Dashboard/Komponen/SideNavComponent";

export default function RegisterPage() {
  useEffect(() => {
    if (localStorage.getItem("user-info")) {
      navigate("/dashboard");
    }
  }, []);
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const navigate = useNavigate();

  async function signUp() {
    let item = { name, password, email };
    console.warn(item);

    let result = await fetch("http://localhost:8000/api/register", {
      method: "POST",
      body: JSON.stringify(item),
      headers: {
        "content-type": "application/json",
        Accept: "application/json",
      },
    });
    result = await result.json();
    console.warn("result", result);
    localStorage.setItem("user-info", JSON.stringify(result));
    navigate("/dashboard");
  }
  return (
    <>
    <div className="h-screen w-screen flex items-center justify-center bg-gray-800">
      <div className="bg-white flex flex-col p-12 w-[40%]">
        <h1 className="font-roboto text-5xl mb-4">Create New Account</h1>
        <p className="text-lg font-poppins text-black">Already have an account? {' '}
            <span className="text-blue-400 underline font-poppins text-lg cursor-pointer" onClick={() => navigate('/login')}>Sign in</span>
          </p>
        <FormControl className="mb-4">
          <FormLabel htmlFor="name">Your name</FormLabel>
          <Input id="name" type="text" onChange={(e) => setName(e.target.value)} />
        </FormControl>
        <FormControl className="mb-4">
          <FormLabel htmlFor="email">Email address</FormLabel>
          <Input id="email" type="email" onChange={(e) => setEmail(e.target.value)}/>
        </FormControl>
        <FormControl className="mb-8">
          <FormLabel htmlFor="password">Password</FormLabel>
          <Input id="password" type="password" onChange={(e) => setPassword(e.target.value)}/>
        </FormControl>
        <Button
          onClick={signUp}
          variant="outline"
          bgColor="#B5CDF5"
          _hover={{ bgColor: "#94B7F2" }}
        >
          Register
        </Button>
      </div>
    </div>
    </>
  );
}
